# flask-rest-github

Simple REST service which returns details of given Github repository.


## Getting Started

The API of the service work with:
```
GET /repositories/{owner}/{repository-name}
```
Where {owner} is Github username and {repository-name} is repository name which we are looking for.


### Prerequisites

What things you need to install the software and how to install them

```
Python3
Python-pip
Flask
Requests
```

### Installing

First off.

```
git clone https://lmalkowski@bitbucket.org/lmalkowski/flask-rest-github.git
cd flask-rest-github/
```

Before running you should install used libraries.

Please update your env with:

```
sudo apt-get install python3
sudo apt-get install python-pip
pip install -r requirements.txt
```


## Running the application

To run dev or prod application type:

```
python3 main.py {dev,prod}

```


## Running the tests

```
python3 -m unittest tests/my_tests.py
```


## Built With

* [FLASK](http://flask.pocoo.org/) - The web framework used
* [REQUESTS](http://docs.python-requests.org/) - Requests library for Python


## Authors

* **Łukasz Małkowski** - [Github](https://github.com/lmalkowski/) [Bitbucket](https://bitbucket.org/lmalkowski/)
