# -*- coding: utf-8 -*-
import app.my_app as Flask
import argparse
from config.dev import DevConfig
from config.prod import ProdConfig


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("env",
                        help="Choose prod or dev environment.",
                        choices=['dev', 'prod'])
    args = parser.parse_args()

    if args.env == 'prod':
        Flask.app.run(debug=ProdConfig.DEBUG,
                      host=ProdConfig.IP,
                      port=ProdConfig.PORT)
    elif args.env == 'dev':
        Flask.app.env = DevConfig.ENV
        Flask.app.run(debug=DevConfig.DEBUG)
