# -*- coding: utf-8 -*-
from flask import Flask
from flask_restful import Api, Resource, reqparse
from flask import request
import requests
import sys
from datetime import datetime, date

app = Flask(__name__)
api = Api(app)


@app.route('/')
def index():
    return 'home of flask-rest-github'


class Github(Resource):
    def __init__(self):
        self.name = ''
        self.repo = ''

    def get(self, name, repo):
        try:
            token = '?access_token=48b71a049480db6dc7166df1dfe17560aadf4c40'
            url = "https://api.github.com/repos/%s/%s%s" % (name, repo, token)
            response = requests.get(url)
            if response.status_code != 200:
                return " status should be 200 ", response.status_code
            data = response.json()
            data_dict = {
                            'fullName': data['name'],
                            'description': data['description'],
                            'cloneUrl': data['clone_url'],
                            'stars': data['stargazers_count'],
                            'createdAt': datetime.strptime(
                                data['created_at'],
                                '%Y-%m-%dT%H:%M:%SZ'
                                ).date().isoformat(),
                        }
            return data_dict, response.status_code
        except requests.exceptions as e:
            return "Requests Error: " + str(type(e)) + " " + str(e), 500
        except Exception as e:
            return "Error: " + str(type(e)) + " " + str(e), 500


api.add_resource(Github,
                 "/repositories/<string:name>/<string:repo>")
