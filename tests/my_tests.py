# -*- coding: utf-8 -*-
import unittest
import sys
from app.my_app import Github


class TestFlaskRestGithub(unittest.TestCase):

    def setUp(self):
        self.frg = Github()

    def testFlaskRestGithubGet404(self):
        name = 'probably_wrong_username'
        repo = 'probably_wrong_repository'
        result = self.frg.get(name, repo)
        if len(result) != 2:
            assert False, "Error"
        assert result[0] == 404,\
            "frg get404 did not set correctly"

    def testFlaskRestGithubGetNot200(self):
        name = 'probably_wrong_username'
        repo = 'probably_wrong_repository'
        result = self.frg.get(name, repo)
        if len(result) != 2:
            assert False, "Error"
        assert result[1] != 200,\
            "frg get not 200 did not set correctly"

    def testFlaskRestGithubGet200(self):
        name = 'lmalkowski'
        repo = 'hello-world'
        result = self.frg.get(name, repo)
        if len(result) != 2:
            assert False, "Error"
        assert result[1] == 200,\
            "frg get200 did not set correctly"


if __name__ == "__main__":
    unittest.main()
